package com.example.officeone.cs.model;

public class CallInfo {

    private String userName;

    private String userPhone;

    private String callType;

    private String time;

    public CallInfo(String userName, String userPhone, String callType, String time) {
        this.userName = userName;
        this.userPhone = userPhone;
        this.callType = callType;
        this.time = time;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
