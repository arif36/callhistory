package com.example.officeone.cs.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.officeone.cs.R;
import com.example.officeone.cs.model.OutgoingCall;

import java.util.List;

public class OutgoingCallAdapter extends RecyclerView.Adapter<OutgoingCallAdapter.OutgoingCallViewHolder> {

    Context context;
    List<OutgoingCall> outgoingCallList;

    public OutgoingCallAdapter(Context context, List<OutgoingCall> list){
        this.context= context;
        this.outgoingCallList = list;
    }


    @NonNull
    @Override
    public OutgoingCallViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.outgoing_call_item,viewGroup,false);
        return new OutgoingCallViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OutgoingCallViewHolder holder,final int position) {
        holder.userName.setText(outgoingCallList.get(position).getUserName());
        holder.userMobile.setText(outgoingCallList.get(position).getUserPhone());
        holder.time.setText(outgoingCallList.get(position).getTime());
        holder.type.setText(outgoingCallList.get(position).getCallType());
        holder.callImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + outgoingCallList.get(position).getUserPhone() ));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return outgoingCallList.size();
    }

    public class OutgoingCallViewHolder extends RecyclerView.ViewHolder {
        TextView userName, userMobile, time,type;
        ImageView callImageButton;

        public OutgoingCallViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.tv_caller_name);
            userMobile = itemView.findViewById(R.id.tv_caller_mobile_number);
            time = itemView.findViewById(R.id.tv_caller_time);
            type = itemView.findViewById(R.id.tv_call_type);
            callImageButton = itemView.findViewById(R.id.image_view_call);

        }

    }
}
