package com.example.officeone.cs.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.officeone.cs.R;
import com.example.officeone.cs.fragments.IncommingCallFragment;
import com.example.officeone.cs.fragments.MessageFragment;
import com.example.officeone.cs.fragments.MissedCallFragment;
import com.example.officeone.cs.fragments.OutgoingCallFragment;

import java.util.ArrayList;
import java.util.List;

public class CallFragmentPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public CallFragmentPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // return null to display only the icon
        //return null;
        return mFragmentTitleList.get(position);
    }
}