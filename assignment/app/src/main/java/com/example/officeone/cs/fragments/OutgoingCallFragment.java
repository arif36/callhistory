package com.example.officeone.cs.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.officeone.cs.MainActivity;
import com.example.officeone.cs.R;
import com.example.officeone.cs.adapter.OutgoingCallAdapter;
import com.example.officeone.cs.model.OutgoingCall;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutgoingCallFragment extends Fragment {

    RecyclerView recyclerView;
    OutgoingCallAdapter adapter;
//    List<OutgoingCall> outgoingCallList;


    public OutgoingCallFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_incomming_call, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
//        outgoingCallList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new OutgoingCallAdapter(getContext(),MainActivity.outgoingCallList);
        recyclerView.setAdapter(adapter);
        return view;
    }

}
