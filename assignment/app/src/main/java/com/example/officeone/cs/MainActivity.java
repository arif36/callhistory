package com.example.officeone.cs;

import android.Manifest;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.officeone.cs.adapter.CallFragmentPagerAdapter;
import com.example.officeone.cs.adapter.CallListAdapter;
import com.example.officeone.cs.fragments.IncommingCallFragment;
import com.example.officeone.cs.fragments.MessageFragment;
import com.example.officeone.cs.fragments.MissedCallFragment;
import com.example.officeone.cs.fragments.OutgoingCallFragment;
import com.example.officeone.cs.model.CallInfo;
import com.example.officeone.cs.model.IncommingCall;
import com.example.officeone.cs.model.MissedCall;
import com.example.officeone.cs.model.OutgoingCall;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{


    String allInfo= "";

    CallListAdapter adapter;
    RecyclerView recyclerView;
    List<CallInfo> callInfoItem;

    public static List<MissedCall> missedCallList;
    public static List<OutgoingCall> outgoingCallList;
    public static List<IncommingCall> incommingCallList;

    CallInfo callInfo;
    MissedCall missedCall;
    OutgoingCall outgoingCall;
    IncommingCall incommingCall;

    ViewPager viewPager;
    TabLayout tabLayout;

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CheckPermission();

    }

    @AfterPermissionGranted(100)
    private void CheckPermission() {
        String[] permissions = {Manifest.permission.READ_CALL_LOG,Manifest.permission.WRITE_CALL_LOG,Manifest.permission.READ_CONTACTS,Manifest.permission.CALL_PHONE};
        if (EasyPermissions.hasPermissions(this,permissions)){

            recyclerView = findViewById(R.id.recyclerView);
            viewPager = findViewById(R.id.viewpager);

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            tabLayout = findViewById(R.id.sliding_tabs);


            callInfoItem = new ArrayList<>();
            missedCallList = new ArrayList<>();
            outgoingCallList = new ArrayList<>();
            incommingCallList = new ArrayList<>();

            getCallDetails();

            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);

//            adapter = new CallListAdapter(this,callInfoItem);
//            recyclerView.setAdapter(adapter);
        }else{
            EasyPermissions.requestPermissions(this,"We need permissions because this and that",100,permissions);
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        CallFragmentPagerAdapter adapter = new CallFragmentPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new IncommingCallFragment(), "Incomming");
        adapter.addFragment(new MissedCallFragment(), "Missed");
        adapter.addFragment(new OutgoingCallFragment(), "Outgoing");
        adapter.addFragment(new MessageFragment(),"Message");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }

    private void getCallDetails() {

        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null,
                null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
//        sb.append("Call Details :");

        while (managedCursor.moveToNext()) {
            String userName = managedCursor.getString(name);

            String phNumber = managedCursor.getString(number);

            String callType = managedCursor.getString(type);

            String callDate = managedCursor.getString(date);
            String callDuration = managedCursor.getString(duration);

            Date callDayTime = new Date(Long.valueOf(callDate));
            String dir = null;
            int dircode = Integer.parseInt(callType);

            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }

//            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
//                    + dir + " \nCall Date:--- " + callDayTime
//                    + " \nCall duration in sec :--- " + callDuration);
//            sb.append("\n----------------------------------");


            String[] timeArray = callDayTime.toString().split(" ");
            String time = timeArray[0]+" "+timeArray[1]+" "+timeArray[2]+" "+timeArray[3];

            if (dir.equals("INCOMING")){
                incommingCall = new IncommingCall(userName,phNumber,dir,time);
                incommingCallList.add(incommingCall);
            }else if(dir.equals("MISSED")){
                missedCall = new MissedCall(userName,phNumber,dir,time);
                missedCallList.add(missedCall);
            }else if(dir.equals("OUTGOING")){
                outgoingCall = new OutgoingCall(userName,phNumber,dir,time);
                outgoingCallList.add(outgoingCall);
            }
//            callInfo = new CallInfo(userName,phNumber,dir,time);
//            callInfoItem.add(callInfo);
        }
        managedCursor.close();

    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE){
            //accepted
            getCallDetails();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this,perms)){
            new AppSettingsDialog.Builder(this).build().show();

        }
    }
}
