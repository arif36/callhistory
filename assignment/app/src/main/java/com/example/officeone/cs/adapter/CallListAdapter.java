package com.example.officeone.cs.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.officeone.cs.R;
import com.example.officeone.cs.model.CallInfo;
import com.example.officeone.cs.model.IncommingCall;
import com.example.officeone.cs.model.MissedCall;
import com.example.officeone.cs.model.OutgoingCall;

import java.util.ArrayList;
import java.util.List;

public class CallListAdapter extends RecyclerView.Adapter<CallListAdapter.CallListViewHolder>{

    List<CallInfo> itemList;
    List<IncommingCall> incommingCallList;
    List<OutgoingCall> outgoingCallList;
    List<MissedCall> missedCallList;
    Context context;



    public CallListAdapter(Context context ,List<CallInfo> list){
        this.context = context;
        this.itemList =  list;
//        incommingCallList= new ArrayList<>();
//        outgoingCallList = new ArrayList<>();
//        missedCallList = new ArrayList<>();
    }

    @NonNull
    @Override
    public CallListAdapter.CallListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.call_item_row,viewGroup,false);
        return new CallListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallListAdapter.CallListViewHolder holder, int position) {
        holder.userName.setText(itemList.get(position).getUserName());
        holder.userMobile.setText(itemList.get(position).getUserPhone());
        holder.time.setText(itemList.get(position).getTime());
        holder.type.setText(itemList.get(position).getCallType());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class CallListViewHolder extends RecyclerView.ViewHolder{

        TextView userName, userMobile, time,type;
//        ImageView imgCall;
        public CallListViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.tv_caller_name);
            userMobile = itemView.findViewById(R.id.tv_caller_mobile_number);
            time = itemView.findViewById(R.id.tv_caller_time);
            type = itemView.findViewById(R.id.tv_call_type);
//            imgCall = itemView.findViewById(R.id.image_view_call);
        }
    }
}
