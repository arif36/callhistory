package com.example.officeone.cs.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.officeone.cs.R;
import com.example.officeone.cs.model.IncommingCall;

import java.util.List;

public class IncommingCallAdapter extends RecyclerView.Adapter<IncommingCallAdapter.IncommingCallViewHolder> {

    Context context;
    List<IncommingCall> incommingCallList;

    public IncommingCallAdapter(Context context, List<IncommingCall> list){
        this.context = context;
        this.incommingCallList = list;
    }

    @NonNull
    @Override
    public IncommingCallAdapter.IncommingCallViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.incomming_call_item,viewGroup,false);
        return new IncommingCallViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IncommingCallAdapter.IncommingCallViewHolder holder, final int position) {
        holder.userName.setText(incommingCallList.get(position).getUserName());
        holder.userMobile.setText(incommingCallList.get(position).getUserPhone());
        holder.time.setText(incommingCallList.get(position).getTime());
        holder.type.setText(incommingCallList.get(position).getCallType());
        holder.callImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + incommingCallList.get(position).getUserPhone() ));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return incommingCallList.size();
    }

    public class IncommingCallViewHolder extends RecyclerView.ViewHolder {
        TextView userName, userMobile, time,type;
        ImageView callImageButton;
        public IncommingCallViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.tv_caller_name);
            userMobile = itemView.findViewById(R.id.tv_caller_mobile_number);
            time = itemView.findViewById(R.id.tv_caller_time);
            type = itemView.findViewById(R.id.tv_call_type);
            callImageButton = itemView.findViewById(R.id.image_view_call);
        }
    }
}
