package com.example.officeone.cs.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.officeone.cs.MainActivity;
import com.example.officeone.cs.R;
import com.example.officeone.cs.adapter.MissedCallAdapter;
import com.example.officeone.cs.model.MissedCall;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MissedCallFragment extends Fragment {


    RecyclerView recyclerView;
    MissedCallAdapter adapter;
//    List<MissedCall> missedCallList;


    public MissedCallFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_missed_call, container, false);


        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MissedCallAdapter(getContext(),MainActivity.missedCallList);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
