package com.example.officeone.cs.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.officeone.cs.R;
import com.example.officeone.cs.model.MissedCall;

import java.util.List;

public class MissedCallAdapter extends RecyclerView.Adapter<MissedCallAdapter.MissedCallViewHolder> {

    Context context;
    List<MissedCall> missedCallList;

    public MissedCallAdapter(Context context, List<MissedCall> list){
        this.context= context;
        this.missedCallList = list;
    }

    @NonNull
    @Override
    public MissedCallViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.missed_call_item,viewGroup,false);
        return new MissedCallViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MissedCallViewHolder holder, final int position) {
        holder.userName.setText(missedCallList.get(position).getUserName());
        holder.userMobile.setText(missedCallList.get(position).getUserPhone());
        holder.time.setText(missedCallList.get(position).getTime());
        holder.type.setText(missedCallList.get(position).getCallType());
        holder.callImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + missedCallList.get(position).getUserPhone() ));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return missedCallList.size();
    }

    public class MissedCallViewHolder extends RecyclerView.ViewHolder {
        TextView userName, userMobile, time,type;
        ImageView callImageButton;
        public MissedCallViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.tv_caller_name);
            userMobile = itemView.findViewById(R.id.tv_caller_mobile_number);
            time = itemView.findViewById(R.id.tv_caller_time);
            type = itemView.findViewById(R.id.tv_call_type);
            callImageButton = itemView.findViewById(R.id.image_view_call);
        }
    }
}
