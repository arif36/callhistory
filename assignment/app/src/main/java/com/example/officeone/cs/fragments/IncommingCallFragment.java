package com.example.officeone.cs.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.officeone.cs.MainActivity;
import com.example.officeone.cs.R;
import com.example.officeone.cs.adapter.CallListAdapter;
import com.example.officeone.cs.adapter.IncommingCallAdapter;
import com.example.officeone.cs.model.CallInfo;
import com.example.officeone.cs.model.IncommingCall;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncommingCallFragment extends Fragment {


    IncommingCallAdapter adapter;
    RecyclerView recyclerView;
//    List<IncommingCall> incommingCallList;

    public IncommingCallFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_incomming_call, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        incommingCallList = new ArrayList<>();
        adapter = new IncommingCallAdapter(getContext(),MainActivity.incommingCallList);
        recyclerView.setAdapter(adapter);
        return view;
    }
}
